<?php

declare(strict_types = 1);

namespace app\component;

use app\interfaces\TaskComponentInterface;
use app\services\Logger;
use LeadGenerator\Lead;

/**
 * Класс TaskComponent реализует метод работы с заданием.
 */
class TaskComponent implements TaskComponentInterface
{
    /**
     * Cвойство хранит атрибут "Заявки" класса "TaskComponent".
     *
     * @var Lead
     */
    protected $lead;
    /**
     * Cвойство хранит атрибут "Заявки" класса "TaskComponent".
     *
     * @var Lead
     */
    protected $logger;

    /**
     * Метод возвращает значение Logger.
     *
     * @return Logger
     */
    public function getLogger(): Logger
    {
        return $this->logger;
    }

    /**
     * Метод устанавливает значение Logger.
     *
     * @param Logger $value Новое значение.
     *
     * @return TaskComponent
     */
    public function setLogger(Logger $value): self
    {
        $this->logger = $value;
        return $this;
    }

    /**
     * Метод возвращает значение "Заявка".
     *
     * @return Lead
     */
    public function getLead(): Lead
    {
        return $this->lead;
    }

    /**
     * Метод устанавливает значение "Заявка".
     *
     * @param Lead $value Новое значение.
     *
     * @return TaskComponentInterface
     */
    public function setLead(Lead $value): TaskComponentInterface
    {
        $this->lead = $value;
        return $this;
    }

    /**
     * Конфтруктор класса Задачи.
     *
     * @param Lead $lead
     */
    public function __construct(Lead $lead, Logger $logger)
    {
        $this->setLead($lead);
        $this->setLogger($logger);
    }

    /**
     * Метод запускает задачу.
     *
     * @return void
     */
    public function runTask(): void
    {
        sleep(2);
        $textAr = [
            $this->getLead()->id,
            $this->getLead()->categoryName,
        ];
        $this->getLogger()->log(1, '', $textAr);
    }
}