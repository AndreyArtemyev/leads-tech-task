<?php

use app\component\HandlerComponent;
use app\component\TaskComponent;
use app\services\Logger;
use app\services\route\FileRoute;
use LeadGenerator\Generator;
use LeadGenerator\Lead;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/services/rout/FileRoute.php';

const COUNT_LEADS   = 10;
const COUNT_THREADS = 2;

$start = microtime(true);

$pool      = new Pool(COUNT_THREADS);
$generator = new Generator();

$logger = new Logger();
$logger->routes->attach(new FileRoute([
    'isEnable' => true,
    'filePath' => 'data/log.text',
]));

$generator->generateLeads(COUNT_LEADS, function(Lead $lead) use ($pool, $logger) {
    $task = new TaskComponent($lead, $logger);
    $pool->submit(new HandlerComponent($task));
});
while ($pool->collect()) {
    $pool->shutdown();
}

printf("Done for %.2f seconds" . PHP_EOL, microtime(true) - $start);
