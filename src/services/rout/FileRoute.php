<?php

namespace app\services\route;

use app\services\Route;

/**
 * Class FileRoute
 */
class FileRoute extends Route
{
    /**
     * @var string Путь к файлу
     */
    public $filePath;
    /**
     * @var string Шаблон сообщения
     */
    public $template = "{context} | {date}";

    /**
     * @inheritdoc
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        if (!file_exists($this->filePath))
        {
            touch($this->filePath);
        }
    }

    /**
     * @inheritdoc
     */
    public function log($level, $message, array $context = [])
    {

        file_put_contents($this->filePath, trim(strtr($this->template, [
                '{level}' => $level,
                '{message}' => $message,
                '{context}' => $this->contextStringify($context),
                '{date}' => $this->getDate(),
            ])) . PHP_EOL, FILE_APPEND);
    }
}