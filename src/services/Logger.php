<?php

declare(strict_types = 1);

namespace app\services;

use SplObjectStorage;

/**
 * Class Logger
 */
class Logger
{
    /**
     * @var SplObjectStorage Список роутов.
     */
    public $routes;

    /**
     * Конструктор класса Logger.
     */
    public function __construct()
    {
        $this->routes = new SplObjectStorage();
    }

    /**
     * @param       $level
     * @param       $message
     * @param array $context
     */
    public function log($level, $message, array $context = [])
    {
        foreach ($this->routes as $route)
        {
            if (!$route instanceof Route)
            {
                continue;
            }
            if (!$route->isEnable)
            {
                continue;
            }
            $route->log($level, $message, $context);
        }
    }
}